Source: imgsizer
Section: web
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               imagemagick,
               python3,
               xmlto,
Standards-Version: 4.7.0
Homepage: http://www.catb.org/~esr/imgsizer/
Vcs-Browser: https://salsa.debian.org/debian/imgsizer
Vcs-Git: https://salsa.debian.org/debian/imgsizer.git
Rules-Requires-Root: no

Package: imgsizer
Architecture: all
Depends: imagemagick | file,
         imagemagick | libjpeg-turbo-progs,
         python3,
         ${misc:Depends},
Description: Adds WIDTH and HEIGHT attributes to IMG tags in HTML files
 The imgsizer script automates away the tedious task of creating and
 updating the extension HEIGHT and WIDTH parameters in HTML IMG tags.
 These parameters help browsers to multi-thread image loading, instead of
 having to load images in strict sequence in order to have each one's
 dimensions available so the next can be placed.  This generally allows
 text on the remainder of the page to load much faster.
